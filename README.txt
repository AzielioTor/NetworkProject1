/**********************************************\
 * Aziel T Shaw - 809000893
 * Created: 28/08/2018
 * Last Updated: 05/09/2018
 * Foundations of Networks - Project 1
\**********************************************/


***** Contents *****
.
|____README.txt
|____wirebug.py
|____pcap
| |____dscp_test.pcap
| |____flag_test.pcap
| |____icmp1.pcap
| |____icmp2.pcap
| |____icmp3.pcap
| |____tcp1.pcap
| |____tcp2.pcap
| |____tcp3.pcap
| |____udp1.pcap
| |____udp2.pcap
|____practice_input
| |____icmp.pcap
| |____icmpf.pcap
| |____tcp.pcap
| |____tcpf.pcap
| |____udp.pcap
| |____udpf.pcap
NOTE: Files in the pcap directory are to test the wirebug program


***** Requirements *****
 - Must have Python 3.x installed
 - Must have access to Bash terminal


***** Running the Program *****
 - In order to run this file you need your own pcap files.
 - You can get them by going into wireshark and clicking on a Packet and going: File > Export Specified Packets >
       save as type '.pcap' AND click 'selected packet'.
 - Save file (as pcap) and inside your BASH terminal navigate to the directory where you saved the Packet.
 - Run the command "xxd -px <savedpcap>.pcap > <newpcapfilename>.pcap"
 - Navigate to the directory of your python file in terminal and run the command "python3 wirebug.py <newpcapfilename>.pcap"

NOTE: This program only supports packets of ICMP, TCP, and UDP (All in IPv4)