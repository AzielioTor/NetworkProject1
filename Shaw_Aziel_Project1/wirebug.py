'''
Aziel T Shaw (AzielioTor) - 803000893
Created: 28/08/2018
Last Updated: 05/09/2018
Foundations of Networks - Project 1
'''

# Imports
import sys
import fnmatch as fnm
import socket

def ethernet_header(data):
    print("ETHER:  ----- Ethernet Header -----")
    print("ETHER:")
    # Ethernet header content
        # Packet Size
    print("ETHER:  Packet Size = " + str(len(data)) + " bytes")
        # Destination
    destination = data[0] + ":" + data[1] + ":" + data[2] + ":" + data[3] + ":" + data[4] + ":" + data[5]
    print("ETHER:  Destination = " + destination)
        # Source
    source = data[6] + ":" + data[7] + ":" + data[8] + ":" + data[9] + ":" + data[10] + ":" + data[11]
    print("ETHER:  Source      = " + source)
        # Ethertype
    ethertype = str(data[12] + data[13])
    print("ETHER:  Ethertype   = " + ethertype)
    # End Ethernet header content
    print("ETHER:")
    if ethertype == "0800":
        protocol = ipv4_header(data)
    elif ethertype == "86dd":
        protocol = ipv6_header(data)
    else:
        print("Ethertype not recognized. Exiting.")
        sys.exit(1)
    # Check protocol and launch method for that header
    if int(protocol) == 6:
        tcp_header(data)
    elif int(protocol) == 17:
        udp_header(data)
    elif int(protocol) == 1:
        icmpv4_header(data)
    elif int(protocol) == 58:
        icmpv6_header(data)
    else: # If no protocol is recognized, exit program.
        print("Protocol not recognized. Exiting.")
        sys.exit(1)
    return

'''
Prints the information of the IPv4 header from the packets data
'''
def ipv4_header(data):
    print("IP:     ----- IPHeader -----")
    print("IP:     ")
    print("IP:     Version = 4")
    # IP header content
        # Get data info. Start from 14 for IP header
        # Information includes Header length, the Differentiated services field, the total length, ID, IP Flags,
        # time to live info, and header checksum
        # Header Length
    header_length = int(data[14][1]) * 4
    # Differentiated Services
    dsf1, dsf2, dsf3 = differentiated_services_field(data[15])
    # Total length
    total_length = hex_to_decimal(str(data[16] + data[17]))
    # Identification
    identification = hex_to_decimal(str(data[18] + data[19]))
    # IPv4 Flags
    flag1, flag2, flag3, flag4, flag5 = ipv4_header_flags((str(data[20] + data[21])))
    # Time to Live
    time_to_live = hex_to_decimal(data[22])
    # Header Checksum
    header_checksum = str(data[24] + data[25])
        # Check the protocol and save to variable for printing, and later use.
    protocol = hex_to_decimal(str(data[23]))
    if int(protocol) == 1:
        protocol_name = "ICMP4"
    elif int(protocol) == 6:
        protocol_name = "TCP"
    elif int(protocol) == 17:
        protocol_name = "UDP"
    elif int(protocol) == 58:
        protocol_name = "ICMP6"
    else:
        protocol_name = "Protocol Not Recognized"
        # Source and Destination Address
    source_addr = hex_to_decimal(data[26])+"."+hex_to_decimal(data[27])+"."+hex_to_decimal(data[28])+"."+hex_to_decimal(data[29])
    dest_addr = hex_to_decimal(data[30])+"."+hex_to_decimal(data[31])+"."+hex_to_decimal(data[32])+"."+hex_to_decimal(data[33])
    # Check source addresses hostname and print
    try:
        source_name = socket.gethostbyaddr(str(source_addr))[0]
    except socket.herror:
        source_name = "(hostname unknown)"
    # Check destination addresses hostname and print
    try:
        dest_name = socket.gethostbyaddr(str(dest_addr))[0]
    except socket.herror:
        dest_name = "(hostname unknown)"
        # Options
    if int(data[14][1]) <= 5:
        options = "No Options"
    else:
        options = "Options"
        # Print all IPv4 Header info
    print("IP:     Header length = " + str(header_length) + " (" + data[14][1] + ")")
    print("IP:     Differentiated Services Field = 0x" + dsf1)
    print("IP:           " + dsf2)
    print("IP:           " + dsf3)
    print("IP:     Total Length = " + str(total_length) + " bytes")
    print("IP:     Identifictation = " + str(data[18] + data[19]) + " (" + str(identification) + ")")
    print("IP:     Flags = 0x" + flag1)
    print("IP:         " + flag2)
    print("IP:         " + flag3)
    print("IP:         " + flag4)
    print("IP:         " + flag5)
    print("IP:     Time to live = " + str(time_to_live))
    print("IP:     Protocol = " + str(protocol) + " (" + protocol_name + ")")
    print("IP:     Header Checksum = 0x" + str(header_checksum))
    print("IP:     Source Address = " + str(source_addr) + ", " + source_name)
    print("IP:     Destination Address = " + str(dest_addr) + ", " + dest_name)
    print("IP:     Options = " + options)
    # End IPheader content
    print("IP:")
    return protocol

'''
Prints the information of the IPv6 header from the packets data
NOT SUPPORTED YET
'''
def ipv6_header(data):
    print("IP:     ----- IPHeader -----")
    print("IP:     ")
    print("IP:     Version = 6")
    # IP header content
        # Get data info
    header_length = ""
    type_of_service = ""
    total_length = ""
    identification = ""
    flags = ""
    frag_offset = ""
    time_to_live = ""
    protocol = ""
    header_checksum = ""
    source_addr = ""

        # Print info

    # End IPheader content
    print("IP:")
    return protocol

'''
Prints the information of the TCP header from the packets data
'''
def tcp_header(data):
    print("TCP:    ----- TCP Header -----")
    print("TCP:")
    # Get information on Source Port, Destination Port, Sequence Number, Acknowledgement Number, the Header Length,
        # the Flag information, Window, Checksum, and Urgent Pointer.
    # Source Port
    source_port = hex_to_decimal(str(data[34] + data[35]))
    # Destination Port
    dest_port = hex_to_decimal(str(data[36] + data[37]))
    # Sequence number of the packet
    seq_num = str(data[38] + data[39] + data[40] + data[41])
    # Acknowledgement number of the packet
    ack_num = str(data[42] + data[43] + data[44] + data[45])
    # Header Length of the packet
    header_length = data[46][0]
    # TCP Flags
    flags1, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, flags2 = tcp_header_flags(data[46][1] + data[47])
    # TCP window
    window = hex_to_decimal(str(data[48] + data[49]))
    # TCP Checksum
    check_sum = str(data[50] + data[51])
    # Urgent Point
    urgent_pnt = str(data[52] + data[53])
    # Start TCP Packet analysis by printing the information
    print("TCP:    Source port = " + source_port)
    print("TCP:    Destination port = " + dest_port)
    print("TCP:    Sequence number = 0x" + seq_num)
    print("TCP:    Acknowledgement number = 0x" + ack_num)
    print("TCP:    Header Length = " + header_length + " (" + str(int(header_length) * 4) + " bytes)")
    print("TCP:    Flags = 0x" + flags1)
    print("TCP:        " + f1)
    print("TCP:        " + f2)
    print("TCP:        " + f3)
    print("TCP:        " + f4)
    print("TCP:        " + f5)
    print("TCP:        " + f6)
    print("TCP:        " + f7)
    print("TCP:        " + f8)
    print("TCP:        " + f9)
    print("TCP:        " + f10)
    print("TCP:        " + flags2)
    print("TCP:    Window = " + window)
    print("TCP:    Checksum = 0x" + check_sum)
    print("TCP:    Urgent pointer = " + urgent_pnt)
    # Finish TCP Packet analysis
    print("TCP:")
    # Start printing the first 64 bytes of TCP Data
    print("TCP:    Data: (first 64 bytes)")
    if len(data) > 42:
        print_data(data, "TCP:    ")
    # End program
    return

'''
Prints the information of the UDP header from the packets data
'''
def udp_header(data):
    print("UDP:    ----- UDP Header -----")
    print("UDP:")
    # Get information of: Source Port, Destination Port, Length, and Checksum of UDP packet
    source_port = hex_to_decimal(str(data[34] + data[35]))
    destination_port = hex_to_decimal(str(data[36] + data[37]))
    length = hex_to_decimal(str(data[38] + data[39]))
    checksum = str(data[40] + data[41])
    # Start UDP Packet analysis
    print("UDP:    Source port = " + str(source_port))
    print("UDP:    Destination port = " + str(destination_port))
    print("UDP:    Length = " + str(length))
    print("UDP:    Checksum = " + str(checksum))
    # Finish UDP Packet analysis
    print("UDP:")
    # Start UDP Data
    print("UDP:    Data: (first 64 bytes)")
    if len(data) > 42:
        print_data(data[42:], "UDP:    ")
    # End program
    return

'''
Prints the header information for the ICMPv4 Protocol
'''
def icmpv4_header(data):
    print("ICMP:   ----- ICMPv4 Header -----")
    print("ICMP:")
    # Get information of ICMP type, code, control messages, and checksum
    type = hex_to_decimal(data[34])
    code = hex_to_decimal(data[35])
    type_code, code_code = get_icmpv4_control_messages(int(type), int(code))
    check_sum = str(data[36] + data[37])
    # Start ICMP analysis
    print("ICMP:   Type = " + type + " (" + type_code + ")")
    print("ICMP:   Code = " + code + " (" + code_code + ")")
    print("ICMP:   Checksum = 0x" + check_sum)
    # End ICMP  analysis
    print("ICMP:")
    # Print ICMP Data
    print("ICMP:   Data: (first 64 bytes)")
    if len(data) > 42:
        print_data(data[42:], "ICMP:   ")
    # End program
    return

'''
Prints the header information for the ICMPv6 Protocol
NOT SUPPORTED YET
'''
def icmpv6_header(data):
    print("ICMPv6 header under construction")
    return

'''
Prints the data values of the packet after the header information is done
'''
def print_data(data, header_type):
    # Choose the smallest number between 64 and the length of the data.
    # As to only print what is available if there is less than 64
    data_num = min(len(data), 64)
    toprint = ""
    # Iterate through each byte
    for i in range(0, data_num):
        # Add byte to print
        toprint = toprint + str(data[i])
        # If we're on the first part of the line, print the header info
        if i % 16 == 0:
            print(header_type, end='')
        # Print byte information
        print(data[i], end='')
        # If we're at the end of the line, print what the byte information is.
        if i % 16 == 15:
            print(", \"" + data_read(toprint) + "\"")
            toprint = ""
        # Otherwise, if we're at an even bit, print a space so the bytes are nicely spaced.
        elif i % 2 == 1:
            print(' ', end='')
    # If we make it to the end of the data, and we printed less than 64 bytes, force the rest of the bytes to get printed.
    if not data_num == 64:
        print(", \"" + data_read(toprint) + "\"")
    print(header_type)
    return

'''
Take the data and convert into ASCII values
'''
def data_read(data):
    data_array = [data[i:i + 2] for i in range(0, len(data), 2)]
    toreturn = ""
    # Loop through array of data
    for i in range(0, len(data_array)):
        # Convert number to a decimal value
        number = int(hex_to_decimal(data_array[i]))
        # If the number is outside of "Acceptable range" print a '.'
            # (Acceptable range being ASCII values that aren't alphanumeric, including special characters.)
        if number < 33 or number > 126:
            toreturn = toreturn + "."
        # Else return the ASCII value of the byte
        else:
            toreturn = toreturn + bytearray.fromhex(data_array[i]).decode()
    return toreturn

'''
Given the DCSP data fields, return the string representation.
'''
def differentiated_services_field(data):
    # Split into bits for easy manipulation and analysis.
    data_size = len(data) * 4
    raw_data = str(bin(int(data, 16))[2:]).zfill(data_size)
    dscp = raw_data[:6]
    ecn = str(raw_data[6:])
    dsf1 = data + " (DSCP: "
    # First 6 bits
    if dscp == "000000":
        dsf2 = "0000 00.. = Differentiated Services Codepoint: Class Selector 0 (Default) (0)"
        dsf1 = dsf1 + "CS0"
    elif dscp == "001000":
        dsf2 = "0010 00.. = Differentiated Services Codepoint: Class Selector 1 (8)"
        dsf1 = dsf1 + "CS1"
    elif dscp == "010000":
        dsf2 = "0010 00.. = Differentiated Services Codepoint: Class Selector 2 (16)"
        dsf1 = dsf1 + "CS2"
    elif dscp == "011000":
        dsf2 = "0110 00.. = Differentiated Services Codepoint: Class Selector 3 (24)"
        dsf1 = dsf1 + "CS3"
    elif dscp == "100000":
        dsf2 = "1000 00.. = Differentiated Services Codepoint: Class Selector 4 (32)"
        dsf1 = dsf1 + "CS4"
    elif dscp == "101000":
        dsf2 = "1010 00.. = Differentiated Services Codepoint: Class Selector 5 (40)"
        dsf1 = dsf1 + "CS5"
    elif dscp == "110000":
        dsf2 = "1100 00.. = Differentiated Services Codepoint: Class Selector 6 (48)"
        dsf1 = dsf1 + "CS6"
    elif dscp == "111000":
        dsf2 = "1110 00.. = Differentiated Services Codepoint: Class Selector 7 (56)"
        dsf1 = dsf1 + "CS7"
    else:
        dsf2 = "ERROR"
        dsf1 = dsf1 + "ERROR"
    # Last 2 bits
    if ecn == "00":
        dsf3 = ".... ..00 = Explicit Congestion Notification: Not ECN-Capable Transport (0)"
        dsf1 = dsf1 + ", ECN: Not-ECT)"
    elif ecn == "10":
        dsf3 = ".... ..10 = Explicit Congestion Notification: ECN Capable Transport (2)"
        dsf1 = dsf1 + ", ECN: ECT(0))"
    elif ecn == "01":
        dsf3 = ".... ..01 = Explicit Congestion Notification: ECN Capable Transport (1)"
        dsf1 = dsf1 + ", ECN: ECT(1))"
    elif ecn == "11":
        dsf3 = ".... ..11 = Explicit Congestion Notification: Congestion Encountered (3)"
        dsf1 = dsf1 + ", ECN: CE)"
    else:
        dsf3 = "ERROR"
        dsf1 = dsf1 + "ERROR"
    return dsf1, dsf2, dsf3

'''
Given IPv4 header data, split into different flags and return information on those flags.
'''
def ipv4_header_flags(data):
    # Split data into bits for easy manipulation
    data_size = len(data) * 4
    raw_data = str(bin(int(data, 16))[2:]).zfill(data_size)
    f1 = data
    # Reserved bit
    if data[0] == "0":
        f2 = "0... .... .... .... = Reserved bit: Not set"
    else:
        f2 = "1... .... .... .... = Reserved bit: Set"
    # Don't Fragment bit
    if data[1] == "0":
        f3 = ".0.. .... .... .... = Don't fragment: Not set"
    else:
        f3 = ".1.. .... .... .... = Don't fragment: Set"
    # More Fragments bit
    if data[2] == "0":
        f4 = "..0. .... .... .... = More fragments: Not set"
    else:
        f4 = "..1. .... .... .... = More fragments: Set"
    # Fragment offset bits, split into sets of 4 so it looks nice
    f5 = "..." + raw_data[3:]
    f5_num = int(raw_data[3:], 10)
    f5 = " ".join([f5[i:i + 4] for i in range(0, len(f5), 4)])
    f5 = f5 + " = Fragment offset: " + str(f5_num)
    return f1, f2, f3, f4, f5

'''
Given tcp header data, split into different flags and return information on those flags.
'''
def tcp_header_flags(data):
    # Split data into bits for easy manipulation
    data_size = len(data) * 4
    raw_data = str(bin(int(data, 16))[2:]).zfill(data_size)
    # Prep flag strings
    flags1 = data + "("
    flags2 = "[TCP Flags: "
    # For flag 1
    f1 = raw_data[:3]
    if f1 == "000":
        f1 = f1 + ". .... .... = Reserved: Not set"
        flags2 = flags2 + "..."
    else:
        f1 = f1 + ". .... .... = Reserved: ERROR"
        flags2 = flags2 + "Err"
        flags1 = flags1 + "|Err"
    # For flag 2
    f2 = raw_data[3]
    if f2 == "0":
        f2 = "...0 .... .... = Nonce: Not set"
        flags2 = flags2 + "."
    else:
        f2 = "...1 .... .... = Nonce: Set"
        flags2 = flags2 + "N"
        flags1 = flags1 + "|Non"
    # For flag 3
    f3 = raw_data[4]
    if f3 == "0":
        f3 = ".... 0... .... = Congestion Window Reduced (CWR): Not set"
        flags2 = flags2 + "."
    else:
        f3 = ".... 1... .... = Congestion Window Reduced (CWR): Set"
        flags2 = flags2 + "C"
        flags1 = flags1 + "|Cwr"
    # For flag 4
    f4 = raw_data[5]
    if f4 == "0":
        f4 = ".... .0.. .... = ECN-Echo: Not set"
        flags2 = flags2 + "."
    else:
        f4 = ".... .1.. .... = ECN-Echo: Set"
        flags2 = flags2 + "E"
        flags1 = flags1 + "|Ecn"
    # For flag 5
    f5 = raw_data[6]
    if f5 == "0":
        f5 = ".... ..0. .... = Urgent: Not set"
        flags2 = flags2 + "."
    else:
        f5 = ".... ..1. .... = Urgent: Set"
        flags2 = flags2 + "U"
        flags1 = flags1 + "|Urg"
    # For flag 6
    f6 = raw_data[7]
    if f6 == "0":
        f6 = ".... ...0 .... = Acknowledgement: Not set"
        flags2 = flags2 + "."
    else:
        f6 = ".... ...1 .... = Acknowledgement: Set"
        flags2 = flags2 + "A"
        flags1 = flags1 + "|Ack"
    # For flag 7
    f7 = raw_data[8]
    if f7 == "0":
        f7 = ".... .... 0... = Push: Not set"
        flags2 = flags2 + "."
    else:
        f7 = ".... .... 1... = Push: Set"
        flags2 = flags2 + "P"
        flags1 = flags1 + "|Psh"
    # For flag 8
    f8 = raw_data[9]
    if f8 == "0":
        f8 = ".... .... .0.. = Reset: Not set"
        flags2 = flags2 + "."
    else:
        f8 = ".... .... .1.. = Reset: Set"
        flags2 = flags2 + "R"
        flags1 = flags1 + "|Rst"
    # For flag 9
    f9 = raw_data[10]
    if f9 == "0":
        f9 = ".... .... ..0. = Syn: Not set"
        flags2 = flags2 + "."
    else:
        f9 = ".... .... ..1. = Syn: Set"
        flags2 = flags2 + "S"
        flags1 = flags1 + "|Syn"
    # For flag 10
    f10 = raw_data[11]
    if f10 == "0":
        f10 = ".... .... ...0 = Fin: Not set"
        flags2 = flags2 + "."
    else:
        f10 = ".... .... ...1 = Fin: Set"
        flags2 = flags2 + "F"
        flags1 = flags1 + "|Fin"
    # Finish flags and return
    flags1 = flags1 + "|)"
    flags2 = flags2 + "]"
    return flags1, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, flags2

'''
Returns the control message for ICMPv4 based on the type number and code number.
NOTE: Does not include any deprecated messages.
'''
def get_icmpv4_control_messages(type, code):
    type_code, code_code = "Error", "Error"
    if type == 0:
        type_code, code_code = "Echo Reply", "Echo Reply"
    elif type == 1 or type == 2:
        type_code, code_code = "Unassigned", "Reserved"
    elif type == 3:
        type_code = "Destination Unreachable"
        if code == 0:
            code_code = "Destination network unreachable"
        elif code == 1:
            code_code = "Destination host unreachable"
        elif code == 2:
            code_code = "Destination protocol unreachable"
        elif code == 3:
            code_code = "Destination port unreachable"
        elif code == 4:
            code_code = "Fragmentation required, and DF flag set"
        elif code == 5:
            code_code = "Source route failed"
        elif code == 6:
            code_code = "Destination network unknown"
        elif code == 7:
            code_code = "Destination host unknown"
        elif code == 8:
            code_code = "Source host isolated"
        elif code == 9:
            code_code = "Network administratively prohibited"
        elif code == 10:
            code_code = "Host administratively prohibited"
        elif code == 11:
            code_code = "Network unreachable for ToS"
        elif code == 12:
            code_code = "Host unreachable for ToS"
        elif code == 13:
            code_code = "Communication administratively prohibited"
        elif code == 14:
            code_code = "Host Precedence Violation"
        elif code == 15:
            code_code = "Precedence cutoff in effect"
    elif type == 4:
        type_code, code_code = "Source Quench [deprecated]", "Source Quench [congestion control]"
    elif type == 5:
        type_code = "Redirect Message"
        if code == 0:
            code_code = "Redirect Datagram for the Network"
        elif code == 1:
            code_code = "Redirect Datagram for the Host"
        elif code == 2:
            code_code = "Redirect Datagram for the ToS & Network"
        elif code == 3:
            code_code = "Redirect Datagram for the ToS & Host"
    elif type == 8:
        type_code, code_code = "Echo Request", "Echo Request (used to ping)"
    elif type == 9:
        type_code, code_code = "Router Advertisement", "Router Advertisement"
    elif type == 10:
        type_code, code_code = "Router discovery/selection/solicitation", "Router discovery/selection/solicitation"
    elif type == 11:
        type_code = "Redirect Message"
        if code == 0:
            code_code = "TTL expired in transit"
        elif code == 1:
            code_code = "Fragment reassembly time exceeded"
    elif type == 12:
        type_code = "Parameter Problem: Bad IP header"
        if code == 0:
            code_code = "Pointer indicates the error"
        elif code == 1:
            code_code = "Missing a required option"
        elif code == 2:
            code_code = "Bad length"
    elif type == 13:
        type_code, code_code = "Timestamp", "Timestamp"
    elif type == 14:
        type_code, code_code = "Timestamp Reply", "Timestamp Reply"
    elif type == 19:
        type_code, code_code = "Reserved", "Reserved for Security"
    elif type >= 20 and type <= 29:
        type_code, code_code = "Reserved", "Reserved for robustness experiment"
    elif type == 40:
        type_code, code_code = "Photuris", "Security failures"
    elif type == 41:
        type_code, code_code = "Experimental", "ICMP for experimental mobility protocols such as Seamoby"
    elif type == 42:
        type_code, code_code = "Extended Echo Request", "No Error"
    elif type == 43:
        type_code = "Reserved"
        if code == 0:
            code_code = "No Error"
        elif code == 1:
            code_code = "Malformed Query"
        elif code == 2:
            code_code = "No Such Interface"
        elif code == 3:
            code_code = "No Such Table Entry"
        elif code == 4:
            code_code = "Multiple Interfaces Satisfy Query"
    elif type >= 44 and type <= 252:
        type_code, code_code = "Unassigned", "Reserved"
    elif type == 253:
        type_code, code_code = "Experimental", "RFC3692-style Experiment 1"
    elif type == 254:
        type_code, code_code = "Experimental", "RFC3692-style Experiment 2"
    elif type == 255:
        type_code, code_code = "Reserved", "Reserved"

    # Return codes
    return type_code, code_code

'''
Converts any HEX number into its DECIMAL number
'''
def hex_to_decimal(num):
    return str(int(num, 16))

'''
Removes junk from beginning of packet file 
'''
def remove_packet_junk(data):
    # Remove all whitespace from file so it's one hex stream
    data = data.replace('\n', '').replace('\r', '').replace(' ', '')
    # Check where the Ethernet header would be
        # if the header is for IPv4 or IPv6 then continue, otherwise strip the first 80 'junk' bits
    maybe_eth_header = str(data[24] + data[25] + data[26] + data[27])
    if not maybe_eth_header == "0800" and not maybe_eth_header == "86dd":
        rtn = data[80:]
    else:
        rtn = data
    return rtn

'''
Main method which launches the packet analyzer
'''
def main():
    # Launch Program
    print("Hello, launching " + sys.argv[0] + "\nStarting packet read/decode\n")
    # If no argument is given tell user how to properly use the program
    if len(sys.argv) < 2:
        print("No file specified to get packet info from!!")
        print("Usage: python3 wirebug.py file.pcap")
        sys.exit(-1)
    # If the filename given isn't a ".packet" file then throw and error
    packet_file_name = sys.argv[1]
    if not fnm.fnmatch(packet_file_name, '*.pcap'):
        print("Wrong file type passed in")
        print("Usage: python3 wirebug.py file.pcap")
        sys.exit(-1)
    # Open file and read contents
    packet_file = open(packet_file_name, "r")
    # If something goes wrong reading the file throw an error.
    if not packet_file.mode == "r":
        print("Something went wrong reading the file.")
        print("Usage: python3 wirebug.py file.pcap")
        sys.exit(-1)
    # Read the contents of the file and print the first 80 digits of hex
    pdata_junk = packet_file.read()
    pdata = remove_packet_junk(pdata_junk)
    # Split raw data into array of data
    pdata_array = [pdata[i:i + 2] for i in range(0, len(pdata), 2)]
    # Start packet analysis
    ethernet_header(pdata_array)

# Launches the main program
if __name__ == "__main__":
    main()
